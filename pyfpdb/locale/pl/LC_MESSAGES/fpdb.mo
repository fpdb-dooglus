��    �      \              �     �     �  0   �  6   �     "  R   ;     �     �  !   �  O   �  J   0  2   {     �     �     �     �  z        �     �     �     �     �            M   0  
   ~  
   �  
   �  
   �  
   �  
   �  
   �  
   �  
   �  
   �     �     �            
          ;   7     s       +   �     �     �     �     �     �     �       q     4   �     �  &   �  #   �          0  3   >  :   r     �     �     �     �  (     .   4     c     l     t     y  k   �  &   �          3  "   K     n     �  	   �  	   �     �     �  F   �           )     J     i  j   n  )   �  *     	   .  L   8     �     �     �     �     �     �     �  /   �  #        2     K     h  Y   �  F   �  |   %     �     �  
   �  
   �  ;   �  7     8   O  =   �  6   �     �                &     ,  
   8     C     V     h     n  	   �     �     �     �     �     �     �     �  0   �  4        H     N  )   ^     �     �     �  
   �     �  &   �  {   �     r  x   �  	             &     A     P     k     y     �     �  o   �  .     $   D  #   i      �     �     �     �  �   �  s   �      �   V   !     g!  	   |!     �!     �!     �!     �!     �!     �!     �!     �!     �!     �!  0   �!  
   +"  #   6"     Z"     n"     {"     �"      �"     �"  8   �"     #     ,#     C#  %   c#  #   �#     �#  !   �#     �#  �   $     �%     �%  8   �%  O   &  *   l&  S   �&     �&     '  )   '  s   G'  K   �'  2   (  %   :(     `(     ~(     �(  �   �(      H)     i)  $   �)     �)     �)     �)     �)  v   �)  
   n*  
   y*  
   �*  
   �*  
   �*  
   �*  
   �*  
   �*  
   �*  
   �*     �*     �*     �*     �*     	+  "   +  B   9+     |+     �+  3   �+  	   �+     �+     �+     ,     ,     +,     2,  �   K,  G   �,     -  3   1-  .   e-     �-     �-  5   �-  1   �-     &.     C.  ,   U.     �.  0   �.  =   �.     /     /     +/     2/  \   D/  3   �/     �/     �/     0  2    0     S0     h0     u0     �0     �0  L   �0     �0  )   1  &   21     Y1  ]   _1  2   �1  3   �1     $2  E   12     w2     z2     �2     �2     �2     �2     �2  5   �2  +   3  +   F3  ,   r3  0   �3  :   �3  6   4  �   B4     �4     �4  	   �4     �4  +   5  =   25  5   p5  4   �5  4   �5     6     %6     :6     B6     H6     Y6     f6     �6     �6     �6     �6     �6     �6     �6     �6     �6     �6     	7  @   7  E   [7     �7     �7  9   �7     �7     8     8     '8     38  *   J8  �   u8     �8  �   9     �9     �9     �9     �9  ,   �9     :     0:     @:     Y:  r   s:  0   �:  :   ;  5   R;  /   �;     �;     �;     �;  w   �;  m   _<  "   �<  o   �<     `=     {=     �=     �=     �=     �=  	   �=     �=     �=  	   �=     �=  
   �=  .   >     2>  "   9>     \>     {>  (   �>     �>  .   �>     ?  8   ?      R?  &   s?  #   �?  -   �?  %   �?     @  !   *@     L@   

Error is:   

Legal stats: 
Any major error will be reported there _only_.
 
Failed to get global lock, it is currently held by %s 
Global lock taken by %s 
Note: error output is being diverted to fpdb-errors.txt and HUD-errors.txt in: %s 
Number of TourneyTypes:  
Number of Tourneys:  
This script is only for windows
 
no gtk directories found in your path - install gtk or edit the path manually
 
python 2.5-2.7 not found, please install python 2.5, 2.6 or 2.7 for fpdb
                               ... writers finished  - press return to continue
  Analyzing Database ...   Cleaning Database ...   Hero's cache starts:   If there already are tables in the database %s on %s they will be deleted and you will have to re-import your histories.
  Rebuilding HUD Cache ...   Rebuilding Indexes ...   Villains' cache starts:  % folded BB to steal % folded SB to steal % folded blind to steal % steal attempted (add _0 to name to display with 0 decimal places, _1 to display with 1, etc)
 <control>A <control>B <control>F <control>G <control>H <control>I <control>O <control>P <control>R <control>T ADDED ADDED CURRENCY ADDON ADDON CHIPS ADDON COST A_bout, License, Copying An invalid DB version or missing tables have been detected. Auto Import Bulk Import CLI for importing hands is GuiBulkImport.py COMMENT COMMENT TIMESTAMP CONFIG FILE ERROR COUNT ADDONS COUNT REBUYS CURRENCY Can't find table %s Cannot open Database Maintenance window because other windows have been opened. Re-start fpdb to use this option. Closing this window will stop the Tournament Tracker Config file Confirm deleting and recreating tables Confirm rebuilding database indexes Connected to SQLite: %s Converting %s Could not find tournament %d in hand %d. Skipping.
 Could not retrieve XID from table xwininfo. xwininfo is %s Create or Recreate _Tables DOUBLE OR NOTHING Database ID for %s not found Database Statistics Database error %s in hand %d. Skipping.
 Dump Database to Textfile (takes ALOT of time) END TIME ENTRIES Edit Enter Tournament Error No.%s please send the hand causing this to fpdb-main@lists.sourceforge.net so we can fix the problem. Example stats, player = %s  hand = %s: FPDB Tournament Entry FPDB Tournament Tracker Failed to add streets. handtext=%s Failed to send hand to HUD: %s Fake HUD Main Window Filename: GUARANTEE Global lock released.
 Graphs GuiStove not found. If you want to use it please install pypoker-eval. Hand Replayer Hand _Replayer (not working yet) Hand logged to hand-errors.txt Help Here is the first line of the hand so you can identify it. Please mention that the error was a ValueError: Import database module: MySQLdb not found Import database module: psycopg2 not found Importing It is not currently possible to select "empty" or anything else to that end. KO Logfile is %s
 MATRIX MATRIX ID PROCESSED MATRIX MATCH ID Maintain Databases NB OF KO No board given. Using Monte-Carlo simulation... No match in XTables for table '%s'. No need to drop indexes. No need to rebuild hudcache. No need to rebuild indexes. Not doing this will likely lead to misbehaviour including fpdb crashes, corrupt data etc. Note that you may not select any stat more than once or it will crash. Note: error output is being diverted to fpdb-error-log.txt and HUD-error.txt. Any major error will be reported there _only_. Number of Hands:  Operating System PLAYER IDS PRIZE POOL Please choose the stats you wish to use in the below table. Please confirm that you want to (re-)create the tables. Please confirm that you want to re-create the HUD cache. Please confirm that you want to rebuild the database indexes. Please see fpdb's start screen for license information Positional Stats Quitting normally RANKS REBUY REBUY CHIPS REBUY COST Rebuild DB Indexes Rebuild HUD Cache Rebuy Ring Player Stats SATELLITE SHOOTOUT SNG SPEED STARTING CHIP COUNT SUB TOURNEY BUY IN SUB TOURNEY FEE Session Stats Started at %s -- %d files to import. indexes: %s Status: Connected to %s database named %s on host %s Stove Stove (preview) Strong Warning - Invalid database version TOTAL ADDONS TOTAL REBUYS TOURNEY NAME TOURNEY NO TOURNEYS PLAYERS IDS There is an error in your config file
 This error is not necessarily fatal but it is strongly recommended that you recreate the tables by using the Database menu. This may take a while. This module was developed and tested with version 2.8.18 of gtk.  You are using version %d.%d.%d.  Your milage may vary. Threads:  Tournament Results Import Tournament _Results Import Tourney Graphs Tourney Insert/Update done Tourney Stats Tourney Type Tourney Viewer Tourney _Viewer Unable to load PyGTK modules required for GUI. Please install PyCairo, PyGObject, and PyGTK from www.pygtk.org. Unknown filter filter_name:'%s' in filter:'%s' User cancelled rebuilding db indexes User cancelled rebuilding hud cache User cancelled recreating tables Version Information: WINNINGS WINNINGS CURRENCY We appear to be running in Windows, but the Windows Python Extensions are not loading. Please install the PYWIN32 package from http://sourceforge.net/projects/pywin32/ You are free to change, and distribute original or changed versions of fpdb within the rules set out by the license Your config file is:  [ERROR] More than 1 Database ID found for %s - Multiple currencies not implemented yet _Auto Import and HUD _Database _Graphs _Help _Import _Import through eMail/IMAP _Log Messages _Main _Maintain Databases _Quit _Statistics _Viewers addPlayer: rank:%s - name : '%s' - Winnings (%s) and others could not find tournament: skipping db error: skipping  eMail Import enter table name to find:  fpdb starting ... fpdb_import: sending hand to hud guidb response was  incrementPlayerWinnings: name : '%s' - Add Winnings (%s) lock already held by: saving updated db data self.window doesn't exist? why? sending finish message queue length = table name %s not found, skipping.
 tournament edit window= waiting for writers to finish ... writers finished already Project-Id-Version: Free Poker Database
POT-Creation-Date: 2011-05-17 03:11+CEST
PO-Revision-Date: 2011-04-07 08:53+0000
Last-Translator: steffen123 <steffen@schaumburger.info>
Language-Team: Polish (Poland) <>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: ENCODING
Generated-By: pygettext.py 1.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 

Błąd to:   

Legal stats: 
Błędy o dużym znaczeniu będą kierowane tylko tam.
 
Nie udało się pobrać blokady ogólnej, jest ona aktualnie używana przez %s 
Blokada ogólna została pobrana przez %s 
uwaga: błąd wyjścia został skierowany do fpdb-errors.txt i HUD-errors.txt w %s 
Liczba rodzajów turnieju:  
Liczba turniejów:  
Ten skrypt działa tylko pod Windowsem.
 
Nie znaleziono katalogów GTK w podanej przez Ciebie ścieżce. Zainstaluj GTK lub wprowadź ścieżkę ręcznie.
 
nie znaleziono python 2.5-2.7, zainstaluj python 2.5, 2.6 or 2.7 dla fpdb
                               ... writers finished  - naciśnij enter, aby kontynuować
  Analizowanie bazy danych...   Czyszczenie bazy danych...   Cache gracza rozpoczyna się:   Jeżeli już istnieją tabele w bazie danych %s na %s zostaną one usunięte i będziesz musiał ponownie zaimportować historię rozdań.
  Odbudowywanie cache'u HUD-a...   Odbudowywanie indeksów...   Cache przeciwnika rozpoczyna się:  Folded BB to Steal Folded SB to Steal folded blind to steal Steal Attempted (dodaj _0 do nazwy, aby nie wyświetlać liczb dziesiętnych; _1, aby wyświetlać 1 liczbę dziesiętną, _2 2 itd.)
 <control>A <control>B <control>F <control>G <control>H <control>I <control>O <control>P <control>R <control>T DODANE ADDED CURRENCY ADDON ŻETONY Z ADDONA KOSZT ADDONA O_ programie, licencja, kopiowanie Wykryto nieprawidłową wersję bazy danych lub brakujące tabele. Import automatyczny Import luzem CLI do importowania rozdań jest w GuiBulkImport.py KOMENTARZ COMMENT TIMESTAMP BŁĄD PLIKU KONFIGURACYJNEGO ZLICZONE ADDONY ZLICZONE REBUYE WALUTA Nie znaleziono tabeli %s Nie włączono okna "Konserwacja bazy danych", ponieważ inne okna były otwarte. Wyłącz i włącz ponownie fpdb, aby użyć tej opcji. Zamknięcie tego okna spowoduje zatrzymanie pracy trackera turniejowego Plik konfiguracyjny Potwierdź usunięcie lub ponowne utworzenie tabel. Potwierdź odbudowywanie indeksów bazy danych Połączono z SQLite: %s konwertowanie %s Nie znaleziono turnieju %d w rozdaniu %d: Pomijanie.
 Nie otrzymano XID z tabeli xwinfo. xwinfo jest %s Stwórz albo odbuduj _tabele DOUBLE OR NOTHING ID bazy danych dla %s nie został znaleziony Statystyki bazy danych Błąd bazy danych %s w rozdaniu %d. Pomijanie.
 Zrzuć bazę danych do pliku tekstowego (zajmuje dużo czasu) GODZINA ZAKOŃCZENIA LICZBA GRACZY Edytuj Wprowadź turniej Błąd nr %s. Wyślij rozdanie powodujące problem na adres fpdb-main@lists.sourceforge.net. Przykładowe statystyki, gracz = %s  rozdanie = %s: FPDB Tournament Entry Tracker turniejowy fpdb Nie dodano ulic. handtext=%s wysłanie rozdania do HUD-a się nie powiodło: %s Fake HUD Main Window nazwa pliku: GWARANTOWANE Uwolniono ogólną blokadę.
 Wykresy Nie znaleziono GuiStove. Jeżeli chcesz go używać zainstaluj pypoker-eval. Odwtarzacz rozdań _Odtwarzacz rozdań (jeszcze nie działa) rozdanie zalogowano do hand-errors.txt Pomoc To jest pierwsza linia rozdania, abyś mógł je rozpoznać. Zauważ, że to był ValueError: import modułu bazy danych: nie znaleziono MySQLdb import modułu bazy danych: nie znaleziono psycopg2 importowanie Niemożliwe jest aktualnie wybrać "puste" ani nic innego w tym celu. KO Plik dziennika jest w %s
 MATRIX ZATWIERDZONO ID MATRIXA MATRIX MATCH ID Konserwacja bazy danych NB OF KO Nie podano stołu. Używanie symulacji Monte-Carlo... Brak dopasowań in XTables dla tabeli '%s'. Nie ma potrzeby usuwania indeksów tabelii. Nie ma potrzeby odbudowywania cache'u HUD-a. Nie ma potrzeby odbudowywania indeksów tabelii. Nie zrobienie tego może prowadzić do błędów programu. Pamiętaj, że nie możesz użyć 1 statystyki 2 razy. Uwaga: błąd wyjściowy został skierowany do fpdb-error-log.txt i HUD-error.txt. Wszystkie poważne błędy będą kierowane tylko tam. Liczba rozdań:  System operacyjny ID GRACZA PULA NAGRÓD Wybierz statystyki, które chcesz używać. Potwierdź chcęć usunięcia lub ponownego utworzenia tabel. Potwierdź chęć ponownego utworzenia cache'u HUD-a. Potwierdź chęć odbudowania indeksów bazy danych. Zobacz ekran startowy fpdb, aby sprawdzić licencję Statystyki pozycyjne normalne wychodzenie MIEJSCA REBUY ŻETONY Z REBUYA KOSZT REBUYA Odbuduj indeksy bazy danych Odbuduj cache HUD-a Rebuy Statystyki gracza SATELITA SHOOTOUT SNG SPEED STARTOWA LICZBA ŻETONÓW WPISOWE DO TURNIEJU OPŁATA KASYNOWA Statystyki sesji Rozpoczęto o %s -- %d rozdań do zaimportowania. indeksów - %s Status: połączono do %s z bazą danych o nazwie %s na komputerze %s Stove Stove (podgląd) Poważne ostrzeżenie - nieprawidłowa wersja bazy danych ŁĄCZNIE ADDONÓW ŁĄCZNIE REBUYÓW NAZWA TURNIEJU NR TURNIEJU ID GRACZY TURNIEJOWYCH Wystąpił błąd w pliku konfiguracyjnym
 Ten błąd nie musi oznaczać niczego poważnego, ale zaleca się odbudowanie tabel, używając odpowiedniej opcji w menu "Baza Danych". To może zająć chwilę. Ten moduł został napisany i przetestowany z GTK w wersji 2.8.18.  Używasz wersji %d.%d.%d.  Twój milage może się różnić. Wątki:  Import wyników turniejowych Import _wyników turnieju Wykresy turniejowe wprowadzono turniej/aktualizacja zakończona Statystyki turniejowe Rodzaj turnieju Przeglądarka turniejowa _Przeglądarka turniejowa Nie załadowano modułów PyGTK wymaganych przez interfejs. Zainstaluj PyCairo, PyGObject i PyGTK z www.pygtk.org. nieznany filtr filter_name: '%s' w filtrze: '%s' Użytkownik anulował odbudowywanie indeksów bazy danych. Użytkownik anulował odbudowywanie cache'u HUD-a...  Użytkownik anulował ponowne utworzenie tabel. Informacja o wersji: WYGRANE WINNINGS CURRENCY Python Extensions dla Windowsa się nie ładują. Zainstaluj paczkę PYWIN32 z http://sourceforge.net/projects/pywin32/ Możesz zmieniać i rozpowszechniać oryginalną lub zmienioną wersje fpdb, przestrzegając reguły licencji Twój plik konfiguracyjny jest w:  [BŁĄD] Znaleziono więcej niż 1 ID bazy danych dla %s - obsługa wielu walut nie została jeszcze wdrożona. _Import automatyczny i HUD Baza _danych _Wykresy _Pomoc _Import Import przez e-mail/IMAP _Dziennik _Plik _Konserwacja bazy danych _Wyjście _Statystyki Narzędzia addPlayer: rank:%s - imi : '%s' - wygrane (%s) i inni nie znaleziono turnieju: pomijanie błąd bazy danych: pomijanie  Import przez e-maila wprowadź nazwę tabeli do znalezienia:  trwa włączanie fpdb... fpdb_import: trwa wysyłanie rozdania do HUD-a odpowiedź od guidb to  incrementPlayerWinnings: imi : '%s' - dodaj wygrane (%s) blokada jest już używana przez zapisywanie zaktulizowanej bazy danych self.window nie istnieje? dlaczego? wysyłanie zakończone - długość kolejki = nie znaleziono stołu %s - pomijanie
 tournament edit window= waiting for writers to finish ... writers finished already 